import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt
import seaborn as sns
import os
import random
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img


source = '../images'
classList = os.listdir(source)
dictionary={}
print("Total number of classes: ", len(classList))
print("")
totalclass=len(classList)


def visualizeData():
    ## Count the number of dogs for each class
    csvcsv=[]
    csvcsv.append(['id', 'breed'])
    count=0
    ## For each class
    for i in range(totalclass):

        #print(classList[i])
        filePath=source+'/'+classList[i]
        fileList = os.listdir(filePath)
        for j in range(len(fileList)):

            row=[]
            row.append(count)
            row.append(classList[i][10:])
            csvcsv.append(row)
            count=count+1


    ## Save the Statics to CSV
    my_df = pd.DataFrame(csvcsv)
    my_df.to_csv('dog_csv.csv', index=False, header=False)


    ## Plot the counting result
    labels = pd.read_csv('dog_csv.csv')
    yy = pd.value_counts(labels['breed'])
    fig, ax = plt.subplots()
    fig.set_size_inches(15, 9)
    sns.set_style("whitegrid")

    ax = sns.barplot(x = yy.index, y = yy, data = labels)
    ax.set_xticklabels(ax.get_xticklabels(), rotation = 90, fontsize = 8)
    ax.set(xlabel='Dog Breed', ylabel='Count')
    ax.set_title('Distribution of Dog breeds')
    plt.show()

    ## Top 5 breeds with highest number of data samples
    print(yy.head())
    print('')

    ## Bottom 5 breeds with lowest number of data samples
    print(yy.tail())
    print('')


def visualizeDataAugumentation():
    datagen = ImageDataGenerator(
            rotation_range=40,
            width_shift_range=0.2,
            height_shift_range=0.2,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True,
            fill_mode='nearest')

    img = load_img('../images/n02085620-Chihuahua/n02085620_7.jpg')
    x = img_to_array(img)
    x = x.reshape((1,) + x.shape)

    i = 0
    for batch in datagen.flow(x, batch_size=1,
                              save_to_dir='.', save_prefix='../An02085620', save_format='jpeg'):
        i += 1
        if i > 20:
            break  # otherwise the generator would loop indefinitely

## 1. Uncomment below to visualizae the Dataset
#visualizeData()


## 2. Uncomment below to view an exmample of Data Augumentation Batches in this challenge
#visualizeDataAugumentation()