import numpy as np
from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras import applications
from keras.utils.np_utils import to_categorical
import matplotlib.pyplot as plt
import math



# dimensions of our images.
img_width, img_height = 224, 224
top_model_weights_path = 'bottleneck_fc_model.h5'
train_data_dir = '../train'
validation_data_dir = '../test'

# number of epochs to train top model
#epochs = 50
batch_size = 16

methodList=["VGG16", "ResNet50", "InceptionV3", "InceptionResNetV2", "VGG19", "Xception"]

def save_bottlebeck_features(methodName):
    # build the VGG16 network

    methodIndex=methodList.index(methodName)

    if(methodIndex==0):
        model = applications.VGG16(include_top=False, weights='imagenet')
    elif(methodIndex==1):
        model = applications.ResNet50(include_top=False, weights='imagenet')
    elif(methodIndex==2):
        model = applications.InceptionV3(include_top=False, weights='imagenet')
    elif(methodIndex==3):
        model = applications.InceptionResNetV2(include_top=False, weights='imagenet')
    elif(methodIndex==4):
        model = applications.VGG19(include_top=False, weights='imagenet')
    elif(methodIndex==5):
        model = applications.Xception(include_top=False, weights='imagenet')

    else:
        assert(0)

    ## For quick test
    datagen = ImageDataGenerator(rescale=1. / 255)

    # #datagen = ImageDataGenerator(rescale=1. / 255,
    #                              rotation_range=40,
    #                              width_shift_range=0.2,
    #                              height_shift_range=0.2,
    #                              shear_range=0.2,
    #                              zoom_range=0.2,
    #                              horizontal_flip=True,
    #                              fill_mode='nearest')


    generator = datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    print(len(generator.filenames))
    print(generator.class_indices)
    print(len(generator.class_indices))

    nb_train_samples = len(generator.filenames)
    num_classes = len(generator.class_indices)
    predict_size_train = int(math.ceil(nb_train_samples / batch_size))
    print(predict_size_train)


    bottleneck_features_train = model.predict_generator(generator, predict_size_train)

    np.save('../bnFeatures/bottleneck_features_'+methodName+'_train224.npy', bottleneck_features_train)
    print(np.shape(bottleneck_features_train))

    generator = datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator.filenames)
    predict_size_validation = int(math.ceil(nb_validation_samples / batch_size))
    print(predict_size_validation)

    bottleneck_features_validation = model.predict_generator(generator, predict_size_validation)
    np.save('../bnFeatures/bottleneck_features_'+methodName+'_test224.npy', bottleneck_features_validation)
    print(np.shape(bottleneck_features_validation))


# Uncomment below to extract features from input for each model

#save_bottlebeck_features("VGG16")
#save_bottlebeck_features("InceptionV3")
#save_bottlebeck_features("InceptionResNetV2")



