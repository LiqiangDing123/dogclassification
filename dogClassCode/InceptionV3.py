import numpy as np
from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras import applications
from keras.utils.np_utils import to_categorical
import matplotlib.pyplot as plt
import math
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.callbacks import ModelCheckpoint

from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam, Adamax, RMSprop
from keras import regularizers


# dimensions of our images.
img_width, img_height = 224, 224
top_model_weights_path = 'bottleneck_fc_model.h5'
train_data_dir = '../train'
validation_data_dir = '../test'

# number of epochs to train top model
#epochs = 50
batch_size = 4


def train_top_model():

    datagen_top = ImageDataGenerator(rescale=1. / 255)
    generator_top = datagen_top.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=False)

    nb_train_samples = len(generator_top.filenames)
    num_classes = len(generator_top.class_indices)

    # save the class indices to use use later in predictions
    np.save('train_class_indices.npy', generator_top.class_indices)


    # load the bottleneck features saved earlier
    train_data = np.load('../bnFeatures/bottleneck_features_InceptionV3_train224.npy')

    print(np.shape(train_data))
    # get the class lebels for the training data, in the original order



    # convert the training labels to categorical vectors
    train_labels = generator_top.classes
    train_labels = to_categorical(train_labels, num_classes=num_classes)
    generator_top = datagen_top.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator_top.filenames)

    validation_data = np.load('../bnFeatures/bottleneck_features_InceptionV3_test224.npy')
    validation_labels = generator_top.classes
    validation_labels = to_categorical(validation_labels, num_classes=num_classes)

    np.save('test_class_indices.npy', generator_top.class_indices)


    # InceptionV3
    model = Sequential()
    model.add(GlobalAveragePooling2D(input_shape=train_data.shape[1:]))
    #model.add(Flatten(input_shape=train_data.shape[1:]))

    model.add(Dense(150, activation='relu'))
    model.add(Dropout(0.4))
    model.add(Dense(120, activation='softmax'))

    model.summary()
    adam = Adamax(lr=0.002)
    ##rmsprop
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    checkpointer = ModelCheckpoint(filepath='../model/InceptionV3_fc_weights.hdf5',
                                   verbose=1, save_best_only=True)

    history = model.fit(train_data, train_labels,
                        epochs=30,
                        batch_size=64,
                        callbacks=[checkpointer],
                        validation_data=(validation_data, validation_labels))

    #model.save_weights(top_model_weights_path)

    (eval_loss, eval_accuracy) = model.evaluate(
        validation_data, validation_labels, batch_size=batch_size, verbose=1)

    print("[INFO] accuracy: {:.2f}%".format(eval_accuracy * 100))
    print("[INFO] Loss: {}".format(eval_loss))

    plt.figure(1)

    # summarize history for accuracy

    plt.subplot(211)
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')

    # summarize history for loss

    plt.subplot(212)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

train_top_model()