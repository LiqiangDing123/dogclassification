from sklearn.datasets import load_files
from keras.utils import np_utils
import numpy as np
from glob import glob
from keras.preprocessing import image
from PIL import ImageFile
from tqdm import tqdm
import os


from sklearn.model_selection import train_test_split, ShuffleSplit


## To Generate train/test_tensor for CNNfromScratch only


# define function to load train, test, and validation datasets
def load_dataset(path):
    data = load_files(path)
    dog_files = np.array(data['filenames'])
    dog_targets = np_utils.to_categorical(np.array(data['target']), 120)
    return dog_files, dog_targets

def path_to_tensor(img_path):
    # loads RGB image as PIL.Image.Image type
    img = image.load_img(img_path, target_size=(120, 120))
    # convert PIL.Image.Image type to 3D tensor with shape (224, 224, 3)
    x = image.img_to_array(img)
    # convert 3D tensor to 4D tensor with shape (1, 224, 224, 3) and return 4D tensor
    return np.expand_dims(x, axis=0)

def paths_to_tensor(img_paths):
    list_of_tensors = [path_to_tensor(img_path) for img_path in tqdm(img_paths)]
    return np.vstack(list_of_tensors)

# load train, test, and validation datasets
#train_files, train_targets = load_dataset('../train')
#X_trainvalid, Y_trainvalid = load_dataset('../images')
#test_files, test_targets = load_dataset('../test')
X_traintest, Y_traintest = load_dataset('../images')

ss=ShuffleSplit(n_splits=2, test_size=0.1, random_state=1)
for train_index, test_index in ss.split(X_traintest,Y_traintest):
    train_files, test_files=X_traintest[train_index],X_traintest[test_index]
    train_targets, test_targets=Y_traintest[train_index],Y_traintest[test_index]
    break

# load list of dog names
dog_names = [item[10:-1] for item in sorted(glob("../images/*/"))]

# print statistics about the dataset
print('There are %d total dog categories.' % len(dog_names))
print(dog_names)
#print('There are %d total dog images.\n' % len(np.hstack([train_files, valid_files, test_files])))
print('There are %d training dog images.' % len(train_files))
#print('There are %d validation dog images.' % len(valid_files))
#print('There are %d test dog images.'% len(test_files))

ImageFile.LOAD_TRUNCATED_IMAGES = True

# pre-process the data for Keras
train_tensors = paths_to_tensor(train_files).astype('float32')/255
#valid_tensors = paths_to_tensor(valid_files).astype('float32')/255
test_tensors = paths_to_tensor(test_files).astype('float32')/255

if not os.path.exists('../imageInTensorGood'):
    print("Create imageInTensorGood")
    os.makedirs('../imageInTensorGood')


np.save('../imageInTensorGood/train_tensors.npy', train_tensors)
np.save('../imageInTensorGood/train_targets.npy', train_targets)


# np.save('../imageInTensor/valid_tensors.npy', valid_tensors)
# np.save('../imageInTensor/valid_targets.npy', valid_targets)

np.save('../imageInTensorGood/test_tensors.npy', test_tensors)
np.save('../imageInTensorGood/test_targets.npy', test_targets)


