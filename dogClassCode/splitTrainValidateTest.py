import shutil
import os
import keras
import random

def split_list(a_list):
    pivot = int(len(a_list)/10)
    return a_list[:pivot], a_list[pivot:]

## Total training exmaples: 18571
## Total test examples: 2009

source = '../images'
train_data_dir  = '../train'
test_data_dir = '../test'
trainImageList=[]
testImageList=[]

if not os.path.exists('../train'):
    print("Create train folder")
    os.makedirs('../train')

if not os.path.exists('../test'):
    print("Create test folder")
    os.makedirs('../test')

classList = os.listdir(source)

print("Total number of classes: ", len(classList))
totalclass=len(classList)

## For each class
for i in range(totalclass):
    print("Class ", i)
    filePath=source+'/'+classList[i]
    fileList = os.listdir(filePath)


    ## Create the train and test folder to save the images
    trainClass=train_data_dir+'/'+classList[i]
    testClass=test_data_dir+'/'+classList[i]

    if os.path.exists(trainClass):
        shutil.rmtree(trainClass)
    os.makedirs(trainClass)

    if os.path.exists(testClass):
        shutil.rmtree(testClass)
    os.makedirs(testClass)

    ## Seperate Images to train and test folder
    classImageNum=len(fileList)
    testImage, trainImage=split_list(fileList)
    print("\t Number of Train Images: ", len(trainImage))
    print("\t Number of Test Images:", len(testImage))

    trainImageList.append(len(trainImage))
    testImageList.append(len(testImage))

    ## move some images to train folder
    for j in range(len(trainImage)):

       src=filePath+'/'+trainImage[j]
       dest=trainClass
       shutil.copy2(src, dest)

    ## move some images to test folder
    for j in range(len(testImage)):

       src=filePath+'/'+testImage[j]
       dest=testClass
       shutil.copy2(src, dest)


print(trainImageList)
print(testImageList)
print("Finish Moving...")


## Number of example images in each class
## Training [137, 167, 227, 135, 193, 170, 177, 155, 155, 216, 158, 176, 169, 154, 144, 138, 142, 134, 136, 197, 164, 169, 170, 177, 136, 180, 209, 144, 140, 148, 164, 155, 162, 153, 155, 167, 148, 142, 178, 182, 182, 178, 177, 162, 164, 139, 142, 140, 143, 186, 165, 141, 153, 168, 137, 136, 135, 154, 151, 137, 139, 145, 140, 138, 137, 135, 144, 135, 144, 136, 135, 135, 139, 135, 135, 137, 138, 139, 153, 142, 138, 135, 135, 137, 137, 135, 166, 152, 197, 136, 182, 136, 141, 137, 144, 141, 153, 135, 161, 173, 135, 189, 180, 189, 176, 192, 197, 198, 177, 143, 138, 163, 140, 136, 140, 144, 140, 141, 135, 153]
## Testing [15, 18, 25, 14, 21, 18, 19, 17, 17, 23, 17, 19, 18, 17, 15, 15, 15, 14, 15, 21, 18, 18, 18, 19, 15, 20, 23, 16, 15, 16, 18, 17, 17, 16, 17, 18, 16, 15, 19, 20, 20, 19, 19, 18, 18, 15, 15, 15, 15, 20, 18, 15, 16, 18, 15, 15, 15, 17, 16, 15, 15, 16, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 16, 15, 15, 15, 15, 15, 15, 15, 18, 16, 21, 15, 20, 15, 15, 15, 15, 15, 17, 15, 17, 19, 15, 20, 20, 21, 19, 21, 21, 21, 19, 15, 15, 18, 15, 15, 15, 15, 15, 15, 15, 16]
