import random
import os
import numpy as np
import matplotlib.pyplot as plt
from keras import regularizers

from keras.optimizers import Adam, Adamax

from keras.utils.np_utils import to_categorical

from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.layers.normalization import BatchNormalization

import numpy as np
from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras import applications
from keras.utils.np_utils import to_categorical
import matplotlib.pyplot as plt
import math


# dimensions of our images.
img_width, img_height = 224, 224
top_model_weights_path = 'bottleneck_fc_model.h5'
train_data_dir = '../train'
validation_data_dir = '../test'

# number of epochs to train top model
#epochs = 50
batch_size = 16

methodList=["VGG16", "ResNet50", "InceptionV3", "InceptionResNetV2", "VGG19", "Xception"]

def save_bottlebeck_features(methodName):
    # build the VGG16 network

    methodIndex=methodList.index(methodName)

    if(methodIndex==0):
        model = applications.VGG16(include_top=False, weights='imagenet')
    elif(methodIndex==1):
        model = applications.ResNet50(include_top=False, weights='imagenet')
    elif(methodIndex==2):
        model = applications.InceptionV3(include_top=False, weights='imagenet')
    elif(methodIndex==3):
        model = applications.InceptionResNetV2(include_top=False, weights='imagenet')
    elif(methodIndex==4):
        model = applications.VGG19(include_top=False, weights='imagenet')
    elif(methodIndex==5):
        model = applications.Xception(include_top=False, weights='imagenet')

    else:
        assert(0)

    print("Starting Extracting Features")
    ## For quick test
    datagen = ImageDataGenerator(rescale=1. / 255)

    # #datagen = ImageDataGenerator(rescale=1. / 255,
    #                              rotation_range=40,
    #                              width_shift_range=0.2,
    #                              height_shift_range=0.2,
    #                              shear_range=0.2,
    #                              zoom_range=0.2,
    #                              horizontal_flip=True,
    #                              fill_mode='nearest')


    generator = datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    print(len(generator.filenames))
    print(generator.class_indices)
    print(len(generator.class_indices))

    nb_train_samples = len(generator.filenames)
    num_classes = len(generator.class_indices)
    predict_size_train = int(math.ceil(nb_train_samples / batch_size))
    print(predict_size_train)


    bottleneck_features_train = model.predict_generator(generator, predict_size_train)

    np.save('../bnFeatures/bottleneck_features_'+methodName+'_train224.npy', bottleneck_features_train)
    print(np.shape(bottleneck_features_train))

    print("Starting Extracting Features for testing")
    generator = datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator.filenames)
    predict_size_validation = int(math.ceil(nb_validation_samples / batch_size))
    print(predict_size_validation)

    bottleneck_features_validation = model.predict_generator(generator, predict_size_validation)
    np.save('../bnFeatures/bottleneck_features_'+methodName+'_test224.npy', bottleneck_features_validation)
    print(np.shape(bottleneck_features_validation))

def test_cnnFromScatch():
    ## This test_tensor is in size of (120, 120)

    test_tensors = np.load('../imageInTensorGood/test_tensors.npy')
    test_targets = np.load('../imageInTensorGood/test_targets.npy')

    test_tensors = test_tensors[0:(np.shape(test_tensors)[0] - 10)]
    test_targets = test_targets[0:(np.shape(test_targets)[0] - 10)]

    print("\t test_tensors: ", np.shape(test_tensors))
    print("\t test_targets: ", np.shape(test_targets))

    model = Sequential()
    model.add(BatchNormalization(input_shape=(120, 120, 3)))
    model.add(Conv2D(filters=16, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
    model.add(MaxPooling2D(pool_size=2))
    model.add(BatchNormalization())

    model.add(Conv2D(filters=32, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
    model.add(MaxPooling2D(pool_size=2))
    model.add(BatchNormalization())

    model.add(Conv2D(filters=64, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
    model.add(MaxPooling2D(pool_size=2))
    model.add(BatchNormalization())

    model.add(Conv2D(filters=128, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
    model.add(MaxPooling2D(pool_size=2))
    model.add(BatchNormalization())

    model.add(Conv2D(filters=256, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
    model.add(MaxPooling2D(pool_size=2))
    model.add(BatchNormalization())
    model.add(GlobalAveragePooling2D())
    model.add(Dense(120, activation='softmax'))
    model.summary()

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

    model.load_weights('../model/cnnFromScratch_weights_bestaugmented.hdf5')

    # get index of predicted dog breed for each image in test set
    dog_breed_predictions = [np.argmax(model.predict(np.expand_dims(tensor, axis=0))) for tensor in test_tensors]

    # report test accuracy
    test_accuracy = 100 * np.sum(np.array(dog_breed_predictions) == np.argmax(test_targets, axis=1)) / len(
        dog_breed_predictions)
    print('Test accuracy: %.4f%%' % test_accuracy)

def test_InceptionResNetV2():

    print("")
    print("Testing InceptionResNetV2")

    ## Load Testing Images and Label
    validation_data_dir = '../test'
    datagen_top = ImageDataGenerator(rescale=1. / 255)
    generator_top = datagen_top.flow_from_directory(
        validation_data_dir,
        target_size=(224, 224),
        batch_size=16,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator_top.filenames)

    if not os.path.exists('../bnFeatures'):
        print("Create bnFeatures")
        os.makedirs('../bnFeatures')

    print("bnFeatures Folder Found")

    if(os.path.exists('../bnFeatures/bottleneck_features_InceptionResNetV2_test224.npy')==0):
        print("No Bottleneck Features Found")
        print("Extracting Bottleneck Features from Exisitng Model by Feeding images")
        print("This process takes only one time. Future Testing will use saved version")
        print("Please Wait...")
        save_bottlebeck_features("InceptionResNetV2")

    print("Bottleneck Features Found")

    validation_data = np.load('../bnFeatures/bottleneck_features_InceptionResNetV2_test224.npy')
    validation_labels = generator_top.classes
    validation_labels = to_categorical(validation_labels, num_classes=120)

    ## Create the model and load weights
    model = Sequential()
    model.add(GlobalAveragePooling2D(input_shape=validation_data.shape[1:]))
    model.add(Dense(256, activation='relu', kernel_regularizer=regularizers.l2(0.005)))
    model.add(BatchNormalization())
    model.add(Dense(120, activation='softmax'))

    model.summary()
    adam = Adamax(lr=0.002)
    ##rmsprop
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    model.load_weights('../model/InceptionResNetV2_fc_weights.hdf5')


    (eval_loss, eval_accuracy) = model.evaluate(
        validation_data, validation_labels, batch_size=16, verbose=1)

    print("[INFO] accuracy: {:.2f}%".format(eval_accuracy * 100))
    print("[INFO] Loss: {}".format(eval_loss))
    print("\n")

    show_10_Random_Prediction(model, validation_data, validation_labels)

def test_InceptionV3():

    print("")
    print("Testing InceptionV3")

    ## Load Testing Images and Label
    validation_data_dir = '../test'
    datagen_top = ImageDataGenerator(rescale=1. / 255)
    generator_top = datagen_top.flow_from_directory(
        validation_data_dir,
        target_size=(224, 224),
        batch_size=16,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator_top.filenames)

    if not os.path.exists('../bnFeatures'):
        print("Create bnFeatures")
        os.makedirs('../bnFeatures')

    print("bnFeatures Folder Found")

    if(os.path.exists('../bnFeatures/bottleneck_features_InceptionV3_test224.npy')==0):
        print("No Bottleneck Features Found")
        print("Extracting Bottleneck Features from Exisitng Model by Feeding images")
        print("This process takes only one time. Future Testing will use saved version")
        print("Please Wait...")
        save_bottlebeck_features("InceptionV3")
    print("Bottleneck Features Found")

    validation_data = np.load('../bnFeatures/bottleneck_features_InceptionV3_test224.npy')
    validation_labels = generator_top.classes
    validation_labels = to_categorical(validation_labels, num_classes=120)

    ## Create the model and load weights
    model = Sequential()
    model.add(GlobalAveragePooling2D(input_shape=validation_data.shape[1:]))
    model.add(Dense(150, activation='relu', kernel_regularizer=regularizers.l2(0.005)))
    model.add(Dropout(0.4))
    model.add(Dense(120, activation='softmax'))

    model.summary()
    adam = Adamax(lr=0.002)
    ##rmsprop
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    model.load_weights('../model/InceptionV3_fc_weights.hdf5')


    (eval_loss, eval_accuracy) = model.evaluate(
        validation_data, validation_labels, batch_size=16, verbose=1)

    print("[INFO] accuracy: {:.2f}%".format(eval_accuracy * 100))
    print("[INFO] Loss: {}".format(eval_loss))

    show_10_Random_Prediction(model, validation_data, validation_labels)

def test_VGG16():

    print("")
    print("Testing VGG16")

    ## Load Testing Images and Label
    validation_data_dir = '../test'
    datagen_top = ImageDataGenerator(rescale=1. / 255)
    generator_top = datagen_top.flow_from_directory(
        validation_data_dir,
        target_size=(224, 224),
        batch_size=16,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator_top.filenames)

    if not os.path.exists('../bnFeatures'):
        print("Create bnFeatures")
        os.makedirs('../bnFeatures')

    print("bnFeatures Folder Found")

    if(os.path.exists('../bnFeatures/bottleneck_features_VGG16_test224.npy')==0):
        print("No Bottleneck Features Found")
        print("Extracting Bottleneck Features from Exisitng Model by Feeding images")
        print("This process takes only one time. Future Testing will use saved version")
        print("Please Wait...")
        save_bottlebeck_features("VGG16")
    print("Bottleneck Features Found")


    validation_data = np.load('../bnFeatures/bottleneck_features_VGG16_test224.npy')
    validation_labels = generator_top.classes
    validation_labels = to_categorical(validation_labels, num_classes=120)

    # VGG16
    model = Sequential()
    model.add(GlobalAveragePooling2D(input_shape=validation_data.shape[1:]))
    #model.add(Flatten(input_shape=validation_data.shape[1:]))
    model.add(Dense(512, activation='relu', kernel_regularizer=regularizers.l2(0.005)))
    model.add(BatchNormalization())
    model.add(Dense(256, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(120, activation='softmax'))

    model.summary()
    adam = Adamax(lr=0.002)
    ##rmsprop
    model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    model.load_weights('../model/VGG16_fc_weights.hdf5')


    (eval_loss, eval_accuracy) = model.evaluate(
        validation_data, validation_labels, batch_size=16, verbose=1)

    print("[INFO] accuracy: {:.2f}%".format(eval_accuracy * 100))
    print("[INFO] Loss: {}".format(eval_loss))

    show_10_Random_Prediction(model, validation_data, validation_labels)

def test_Assemble():

    print("")
    print("Testing Assemble Method (VGG16 + InceptionV3)")

    validation_data_dir = '../test'
    datagen_top = ImageDataGenerator(rescale=1. / 255)


    generator_top = datagen_top.flow_from_directory(
        validation_data_dir,
        target_size=(224, 224),
        batch_size=8,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator_top.filenames)

    ## Test Data

    if not os.path.exists('../bnFeatures'):
        print("Create bnFeatures")
        os.makedirs('../bnFeatures')

    print("bnFeatures Folder Found")

    if(os.path.exists('../bnFeatures/bottleneck_features_InceptionV3_test224.npy')==0):
        print("No Bottleneck Features Found")
        print("Extracting Bottleneck Features from Exisitng Model by Feeding images")
        print("This process takes only one time. Future Testing will use saved version")
        print("Please Wait...")
        save_bottlebeck_features("InceptionV3")
    print("Bottleneck Features Found")


    if(os.path.exists('../bnFeatures/bottleneck_features_VGG16_test224.npy')==0):
        print("No Bottleneck Features Found")
        print("Extracting Bottleneck Features from Exisitng Model by Feeding images")
        print("This process takes only one time. Future Testing will use saved version")
        print("Please Wait...")
        save_bottlebeck_features("VGG16")
    print("Bottleneck Features Found")

    inceptionValid = np.load('../bnFeatures/bottleneck_features_InceptionV3_test224.npy')
    inceptionValid = inceptionValid.reshape([2009, 4 * 4 * 2048])
    print("inceptionValid shape: " + str(np.shape(inceptionValid)))

    vgg16Valid = np.load('../bnFeatures/bottleneck_features_VGG16_test224.npy')
    vgg16Valid = vgg16Valid.reshape([2009, 7 * 7 * 512])
    print("vgg16Valid shape: " + str(np.shape(vgg16Valid)))


    validation_data = np.concatenate([inceptionValid, vgg16Valid], axis=-1)
    featureInt = int(np.shape(validation_data)[1] / 2)
    validation_data = validation_data.reshape([2009, 2, featureInt])
    print("validation_data shape: " + str(np.shape(validation_data)))

    # validation_data = np.load('../bnFeatures/bottleneck_features_InceptionV3_test224.npy')
    validation_labels = generator_top.classes
    validation_labels = to_categorical(validation_labels, num_classes=120)

    np.save('test_class_indices.npy', generator_top.class_indices)

    # Inception+ VGG16
    model = Sequential()
    model.add(Flatten(input_shape=validation_data.shape[1:]))
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.5))
    # #model.add(Dense(1024, activation='relu'))
    # #model.add(Dropout(0.5))
    model.add(Dense(120, activation='softmax'))

    model.summary()
    model.compile(optimizer='adam',loss='categorical_crossentropy', metrics=['accuracy'])
    model.load_weights('../model/As_weights_bestaugmented.hdf5')


    (eval_loss, eval_accuracy) = model.evaluate(
        validation_data, validation_labels, batch_size=8, verbose=1)

    print("[INFO] accuracy: {:.2f}%".format(eval_accuracy * 100))
    print("[INFO] Loss: {}".format(eval_loss))

def show_10_Random_Prediction(model, validation_data, validation_labels):
    ## Randomly select 10 images from the testset
    randomlist=[]
    random10=np.zeros((10,np.shape(validation_data)[1], np.shape(validation_data)[2], np.shape(validation_data)[3]))
    label10=np.zeros((10,120))
    for i in range(10):
        randomIndex=random.randint(0, 2008)
        randomlist.append(randomIndex)
        random10[i]=validation_data[randomIndex]
        label10[i]=validation_labels[randomIndex]

    print("Here below are the indexes: ")
    print(randomlist)

    ## Feed this 10 images into model and Get Prediction
    prediction=model.predict_classes(random10, batch_size=2, verbose=0)
    print("Model Prediction: ")
    print(prediction)
    label10=np.argmax(label10, axis=1)
    print("Ground Truth: ")
    print(label10)

    ## Go back to File System to see which 10 images were selected
    source = '../test'
    classList = os.listdir(source)
    totalclass = len(classList)


    classimageList=[]
    count = 0
    ## For each class
    for i in range(totalclass):

        filePath = source + '/' + classList[i]
        fileKey=classList[i]
        fileList = os.listdir(filePath)

        for j in range(len(fileList)):
            row = []
            filename=source+'/'+classList[i]+'/'+fileList[j]
            row.append(filename)
            classimageList.append(row)

    ## Build a dictionary and Convert ID label to world lable
    class_dictionary = np.load('train_class_indices.npy').item()
    inv_map = {v: k for k, v in class_dictionary.items()}


    for i in range(10):
        plt.suptitle("Random 10 Image Selection")
        plt.subplot(2, 5, i + 1)
        eachimage=plt.imread(classimageList[randomlist[i]][0])
        GroundTruth=inv_map[label10[i]][10:]
        OurPrediction=inv_map[prediction[i]][10:]
        plt.title("Image ID: {}\n, Label: {}".format(GroundTruth, OurPrediction))
        plt.imshow(eachimage)
    plt.show()



## !! Make sure you have the corresponding bottleneck features inside bnFeatures folder

## Uncomment below to test each trained model result



## ResnetV2             ## Accuracy around 80%
test_InceptionResNetV2()

## InceptionV3          ## Accruacy around 70%
test_InceptionV3()

## VGG16                ## Accuracy around 30%  (Not finishing fine-tuning)
#test_VGG16()

## Due to the time reason, have not got the time to display images for these two methods
## CNN from scatch      ## Accuracy around 30%
## make sure run ConvertImage2Tensor.py first
#test_cnnFromScatch()

## Assembling method (VGG16+ InceptionV3)
test_Assemble()
