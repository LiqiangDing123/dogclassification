import numpy as np
from keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras import applications
from keras.utils.np_utils import to_categorical
import matplotlib.pyplot as plt
import math
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.callbacks import ModelCheckpoint

from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam, Adamax, RMSprop
from keras import regularizers


# dimensions of our images.
img_width, img_height = 224, 224
top_model_weights_path = 'bottleneck_fc_model.h5'
train_data_dir = '../train'
validation_data_dir = '../test'

# number of epochs to train top model
#epochs = 50
batch_size = 32

def train_top_model():


    datagen_top = ImageDataGenerator(rescale=1. / 255)
    generator_top = datagen_top.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode='categorical',
        shuffle=False)

    nb_train_samples = len(generator_top.filenames)
    num_classes = len(generator_top.class_indices)

    # save the class indices to use use later in predictions
    np.save('train_class_indices.npy', generator_top.class_indices)

    # inception+expection

    inception=np.load('../bnFeatures/bottleneck_features_InceptionV3_train224.npy')
    inception=inception.reshape([18571, 4*4*2048])
    print("inception shape: "+ str(np.shape(inception)))

    vgg16=np.load('../bnFeatures/bottleneck_features_VGG16_train224.npy')
    vgg16=vgg16.reshape([18571, 7*7*512])
    print("vgg16 shape: " + str(np.shape(vgg16)))

    # xception=np.load('../bnFeatures/bottleneck_features_Xception_train22.npy')
    # xception = xception.reshape([18571, 4 * 4 * 2048])
    # print("xception shape: " + str(np.shape(xception)))

    train_data = np.concatenate([inception, vgg16], axis=-1)

    featureInt=int(np.shape(train_data)[1]/2)
    print("featureInt: "+str(featureInt))
    train_data = train_data.reshape([18571, 2, featureInt])
    print("train_data shape: " + str(np.shape(train_data)))

    # load the bottleneck features saved earlier
    #train_data = np.load('../bnFeatures/bottleneck_features_InceptionV3_train224.npy')

    # get the class lebels for the training data, in the original order


    # convert the training labels to categorical vectors
    train_labels = generator_top.classes
    train_labels = to_categorical(train_labels, num_classes=num_classes)
    generator_top = datagen_top.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=batch_size,
        class_mode=None,
        shuffle=False)

    nb_validation_samples = len(generator_top.filenames)


    ## Test Data
    inceptionValid=np.load('../bnFeatures/bottleneck_features_InceptionV3_test224.npy')
    inceptionValid=inceptionValid.reshape([2009, 4*4*2048])
    print("inceptionValid shape: " + str(np.shape(inceptionValid)))

    vgg16Valid=np.load('../bnFeatures/bottleneck_features_VGG16_test224.npy')
    vgg16Valid=vgg16Valid.reshape([2009, 7*7*512])
    print("vgg16Valid shape: " + str(np.shape(vgg16Valid)))

    # xceptionValid = np.load('../bnFeatures/bottleneck_features_Xception_test22.npy')
    # xceptionValid=xceptionValid.reshape([2009, 4 * 4 * 2048])
    # print("xceptionValid shape: " + str(np.shape(xceptionValid)))


    validation_data = np.concatenate([inceptionValid, vgg16Valid], axis=-1)
    validation_data = validation_data.reshape([2009, 2,featureInt])
    print("validation_data shape: " + str(np.shape(validation_data)))

    #validation_data = np.load('../bnFeatures/bottleneck_features_InceptionV3_test224.npy')
    validation_labels = generator_top.classes
    validation_labels = to_categorical(validation_labels, num_classes=num_classes)

    np.save('test_class_indices.npy', generator_top.class_indices)

    # Inception+ Xception
    model = Sequential()
    model.add(Flatten(input_shape=train_data.shape[1:]))
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.5))
    # #model.add(Dense(1024, activation='relu'))
    # #model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation='softmax'))

    model.summary()
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy', metrics=['accuracy'])



    checkpointer = ModelCheckpoint(filepath='../model/As_weights_bestaugmented.hdf5',
                                   verbose=1, save_best_only=True)

    history = model.fit(train_data, train_labels,
                        epochs=30,
                        batch_size=64,
                        callbacks=[checkpointer],
                        validation_data=(validation_data, validation_labels))


    (eval_loss, eval_accuracy) = model.evaluate(
        validation_data, validation_labels, batch_size=batch_size, verbose=1)

    print("[INFO] accuracy: {:.2f}%".format(eval_accuracy * 100))
    print("[INFO] Loss: {}".format(eval_loss))

    plt.figure(1)

    # summarize history for accuracy

    plt.subplot(211)
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')

    # summarize history for loss

    plt.subplot(212)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()


train_top_model()
