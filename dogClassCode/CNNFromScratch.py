from sklearn.datasets import load_files
from keras.utils import np_utils
import numpy as np
import matplotlib.pyplot as plt

from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint
from keras.optimizers import Adam, Adamax
from tqdm import tqdm
import h5py


from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D
from keras.layers import Dropout, Flatten, Dense, Activation
from keras.models import Sequential
from keras.layers.normalization import BatchNormalization


from sklearn.model_selection import train_test_split, ShuffleSplit


train_tensors = np.load('../imageInTensorGood/train_tensors.npy')
train_targets = np.load('../imageInTensorGood/train_targets.npy')

train_tensors=train_tensors[0:(np.shape(train_tensors)[0]-10)]
train_targets=train_targets[0:(np.shape(train_targets)[0]-10)]

print("\t train_tensors: ", np.shape(train_tensors))
print("\t train_targets: ", np.shape(train_targets))


test_tensors = np.load('../imageInTensorGood/test_tensors.npy')
test_targets = np.load('../imageInTensorGood/test_targets.npy')

test_tensors=test_tensors[0:(np.shape(test_tensors)[0]-10)]
test_targets=test_targets[0:(np.shape(test_targets)[0]-10)]

print("\t test_tensors: ", np.shape(test_tensors))
print("\t test_targets: ", np.shape(test_targets))


ss=ShuffleSplit(n_splits=2, test_size=0.1, random_state=1)
for train_index, test_index in ss.split(train_tensors,train_targets):
    train_tensors2, valid_tensors=train_tensors[train_index],train_tensors[test_index]
    train_targets2, valid_targets=train_targets[train_index],train_targets[test_index]
    break



# create and configure augmented image generator
datagen = ImageDataGenerator(
    width_shift_range=0.1,  # randomly shift images horizontally (10% of total width)
    height_shift_range=0.1,  # randomly shift images vertically (10% of total height)
    horizontal_flip=True) # randomly flip images horizontally

# fit augmented image generator on data
datagen.fit(train_tensors)


model = Sequential()
model.add(BatchNormalization(input_shape=(120, 120, 3)))
model.add(Conv2D(filters=16, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
model.add(MaxPooling2D(pool_size=2))
model.add(BatchNormalization())

model.add(Conv2D(filters=32, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
model.add(MaxPooling2D(pool_size=2))
model.add(BatchNormalization())

model.add(Conv2D(filters=64, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
model.add(MaxPooling2D(pool_size=2))
model.add(BatchNormalization())

model.add(Conv2D(filters=128, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
model.add(MaxPooling2D(pool_size=2))
model.add(BatchNormalization())

model.add(Conv2D(filters=256, kernel_size=3, kernel_initializer='he_normal', activation='relu'))
model.add(MaxPooling2D(pool_size=2))
model.add(BatchNormalization())
model.add(GlobalAveragePooling2D())
model.add(Dense(120, activation='softmax'))
model.summary()


model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])



batch_size = 20
epochs = 10

checkpointer = ModelCheckpoint(filepath='../model/cnnFromScratch_weights_bestaugmented.hdf5',
                               verbose=1, save_best_only=True)

### Using Image Augmentation
model.fit_generator(datagen.flow(train_tensors2, train_targets2, batch_size=batch_size),
                    validation_data=(valid_tensors, valid_targets),
                    #validation_split=0.1,
                    steps_per_epoch=train_tensors.shape[0] // batch_size,
                    epochs=epochs, callbacks=[checkpointer], verbose=1)

model.load_weights('../model/cnnFromScratch_weights_bestaugmented.hdf5')

# get index of predicted dog breed for each image in test set
dog_breed_predictions = [np.argmax(model.predict(np.expand_dims(tensor, axis=0))) for tensor in test_tensors]

# report test accuracy
test_accuracy = 100*np.sum(np.array(dog_breed_predictions)==np.argmax(test_targets, axis=1))/len(dog_breed_predictions)
print('Test accuracy: %.4f%%' % test_accuracy)



batch_size = 64
epochs = 30

history = model.fit_generator(datagen.flow(train_tensors2, train_targets2, batch_size=batch_size),
                    validation_data=(valid_tensors, valid_targets),
                    steps_per_epoch=train_tensors.shape[0] // batch_size,
                    epochs=epochs, callbacks=[checkpointer], verbose=1)




# get index of predicted dog breed for each image in test set
dog_breed_predictions = [np.argmax(model.predict(np.expand_dims(tensor, axis=0))) for tensor in test_tensors]

# report test accuracy
test_accuracy = 100*np.sum(np.array(dog_breed_predictions)==np.argmax(test_targets, axis=1))/len(dog_breed_predictions)
print('Test accuracy: %.4f%%' % test_accuracy)




plt.figure(1)
# summarize history for accuracy

plt.subplot(211)
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')

# summarize history for loss

plt.subplot(212)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
