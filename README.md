
##### Hello! Welcome to the Dog Breed Classification Challenge #####
Liqiang.ding@mail.mcgill.ca


## [Recommanded]: To see overall results/explanation 

	please see the ppt slides attached first

	

## To test models, option:[InceptionResNetV2, InceptionV3, VGG16, cnnFromSratch]

	0. Install python dependencies in requirements.txt

	1. unzip the images.tar in the same level of directory as this README.md
	
	2. Create two folders named "train", and "test" in the same directory as this README.md
	
	like this:
	
	root/
		images
		test
		train
		bnFeatures
		results
		dogClassCode
		model
		README.md
	
	
	3. run splitTrainValidateTest.py
	
	
	For [Incpetion, InceptionResnetV2 and VGG16, Assmbel]
	
		4. go to TestModel.py, uncomment the model you want to test
	
		For the first time running only, the program will automatically create a folder called "bnFeatures", 
		Then, it will extract features from test/train folders, and save to bnFeatures based on the model you select
		
		Recommandation: Try with InceptionResnetV2 first
		Warning: VGG16 features take very long to generate
		
		
		This step is necessary since they are the inputs of network.
		I can't upload it directly due to size limit reason. 
		
		

	For [cnnFromSratch]:
	
		4. run ConvertImage2Tensor.py, then go to TestModel.py, uncomment the line corresponding to the cnnFromSratch model

	
## To visualize data 

	1. Go to ViewData.py and uncomment corresponding commands
	
	
	
## To train model

	1. Make sure you did everything in "To run test models " section
	
	2. For CNNFromScratch only, run ConvertImage2Tensor.py first, and then run CNNFromScratch.py
	
	3. For the rest,
	
			go to HighLevelFeatureExtraction.py, uncomment the line for selected model to generate features
			and then run HighLevelFeatureExtraction.py

			## InceptionResnetV2
			run InceptionResNetV2.py

			## VGG16
			run VGG16.py

			## InceptionV3
			run InceptionV3.py
			
			## Assembling Method
			run assemble.py
		
	
## Pleaes Note, you may see the warning below, which sometimes shows up at the end of program. You can ignore it. 
	

	Exception ignored in: <bound method BaseSession.__del__ of <tensorflow.python.client.session.Session object at 0x000001AE9E5E3C18>>
